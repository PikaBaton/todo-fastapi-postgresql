import enum
from datetime import datetime
from app.db_connect import Base
from sqlalchemy import Column, Integer, String, null, Enum, Boolean, DateTime


class Tags(enum.Enum):
    studies = "studies"
    personal = "personal"
    plans = "plans"
    empty = "empty"


class Origins(enum.Enum):
    generated = "generated"
    created = "created"


class Todo(Base):
    __tablename__ = 'todos'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    details = Column(String, nullable=True, default=None)
    tag = Column(Enum(Tags), default="empty")
    origin = Column(Enum(Origins))
    completed = Column(Boolean, default=False)
    created_date = Column(String)
    completed_date = Column(String, nullable=True, default=None)

    def __repr__(self):
        return f'<Todo {self.id}>'
