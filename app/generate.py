import string
import random
from app.shemas import CreateTodo
from app.todos import generate_todo


def gen(count = 20):
    letters = string.ascii_lowercase
    list_tag = ["studies", "personal", "plans", "empty"]
    for i in range(count):
        size = random.randint(5, 20)
        title = ''.join(random.choice(letters) for i in range(size))
        details = ''.join(random.choice(letters) for i in range(size))
        tag = list_tag[random.randint(0, 3)]

        generate_todo(CreateTodo(title=title, details=details, tag=tag, origin="generated"))
