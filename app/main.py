import os
import logging
import openpyxl
import app.db_models

from typing import Optional
from starlette import status
from starlette.responses import FileResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from fastapi import FastAPI, Request, Response, UploadFile, File, Depends, Form, status, Header
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from app.db_connect import engine
from app.todos import *
from app.generate import gen
from app.shemas import CreateTodo, UpdateTodo


server = FastAPI()
app.db_models.Base.metadata.create_all(engine)
os.chdir("app")


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="../main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))


templates = Jinja2Templates(directory="templates")
server.mount("/app/static", StaticFiles(directory="static"), name="static")


@server.get("/")
async def home(request: Request):
    """
    Function for main page
    :param request: request
    :return: HTML of main page 
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    todos = get_todos()[::-1]
    count_of_todos = len(todos)
    return templates.TemplateResponse("main.html", {"request": request, 
                                                    "todos": todos, 
                                                    "count_of_todos": count_of_todos})


@server.post("/add")
async def todo_add(request: Request, 
                   title: str = Form(...), 
                   details: Optional[str] = Form(None),
                   tag: str = Form("empty")):
    """
    Function for adding new todo
    :param request: request
    :param title: title of new todo
    :param details: description of todo
    :param tad: tag of new news
    :return: redirect to main page
    """
    try:
        model = CreateTodo(title=title, details=details, tag=tag, origin="created")
        todo = create_todo(model)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                    f' todo created successful with params title = {title}, details = ' + 
                    f' {details} + tag = {tag}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                     f'Error in creating todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.post("/edit/{todo_id}")
async def todo_edit(request: Request,
                    todo_id: int,
                    title: str = Form(...),
                    details: str = Form(...),
                    tag: str = Form(...),
                    completed: bool = Form(False)):
    """
    Function for edding todo
    :param request: request
    :param todo_id: id of todo
    :param title: title of todo
    :param details: description of todo
    :param tag: tag of todo
    :param completed: info if complited todo
    :return: redirect to main page
    """
    try:
        todo = update_todo(UpdateTodo(id=todo_id, title=title, details=details, tag=tag, completed=bool(completed)))
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                    f" Editting todo: {todo}")
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                     f'Error in edditing todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.delete("/delete/{todo_id}")
async def todo_delete(request: Request, todo_id: int):
    """
    Function for deleting todo
    :param request: request
    :param todo_id: id of deleting todo
    :return: redirect to main page
    """
    try:
        todo = delete_todo(todo_id)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                    f" Deleting todo: {todo}")
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                     f'Error in deleting todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get("/compile/{todo_id}")
async def export(request: Request, todo_id: int):
    """
    Function for completing todo by id
    :param request: request
    :param todo_id: id of todo
    :return: redirect to main page
    """
    try:
        complete_todo(todo_id)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                f"complete todo: {todo_id}")
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                     f'Error in competing todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get("/generate/{count}")
async def generate(request: Request, count: int):
    """
    Function for generate todo by count
    :param request: request
    :param count: count of generating todo
    :return: redirect to main page
    """
    gen(count)
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                f' generating {count} todos')
    return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get('/excel')
async def excel(request: Request):
    """
    Function for rendering page for import/export from excel
    :param request: request
    :return: excel.html
    """
    return templates.TemplateResponse("excel.html", {"request": request})


@server.post("/import")
async def importer(request: Request, file_upload: UploadFile = File(...)):
    """
    Function for import todos from excel
    :param request: request
    :param file_upload: excel file
    :return: new todos
    """
    try:
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                    f' Importing {file_upload.filename}')
        files = import_xlsx(file_upload.filename)
        return files
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' + 
                     f'Error in importing todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get("/export/")
async def exporter(request: Request):
    """
    Function for download excel file with all todos
    :param request: request
    :return: excel file
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" exporting excel')
    export_xlsx()
    return FileResponse(path='todo.xlsx', filename='todo.xlsx', media_type='multipart/form-data')


@server.get("/visualize/")
async def visual(request: Request):
    """
    Function for visualizate todos
    :param request: request
    :return: visual.html
    """
    logger.info("visualize")
    vis()
    return templates.TemplateResponse("visual.html", {"request": request})


@server.get("/todo/{todo_id}")
async def todo_get(request: Request, todo_id: int):
    """
    Function for edditing page
    :param request: request
    :param todo_id: id of todo
    :return: edit.html
    """
    todo = get_todo(todo_id)
    logger.info(f"Getting todo: {todo}")
    return templates.TemplateResponse("edit.html", {"request": request, "todo": todo})