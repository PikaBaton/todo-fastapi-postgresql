from datetime import datetime

import app.db_models
from app.db_connect import SessionLocal
from app.db_models import Todo
from app.shemas import CreateTodo, UpdateTodo
import string
import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime as dt
import csv
import xlsxwriter
import openpyxl


def create_todo(todo: CreateTodo):
    db = SessionLocal()
    td = Todo(title=todo.title,
              details=todo.details,
              tag=todo.tag,
              completed=todo.completed,
              origin=todo.origin,
              completed_date=todo.completed_date,
              created_date=todo.created_date)
    db.add(td)
    db.commit()
    db.refresh(td)
    db.close()
    return td


def generate_todo(todo: CreateTodo):
    db = SessionLocal()
    print(todo.tag)
    td = Todo(title=todo.title, details=todo.details, tag=todo.tag, completed=False, origin=todo.origin)
    db.add(td)
    db.commit()
    db.refresh(td)
    db.close()


def get_todos():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    db.close()
    return todos


def get_todo(id):
    db = SessionLocal()
    todo = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == id).first()
    db.close()
    return todo


def delete_todo(id):
    db = SessionLocal()
    todo = get_todo(id)
    db.delete(todo)
    db.commit()
    db.close()
    return todo


def update_todo(todo: UpdateTodo):
    db = SessionLocal()
    update_file = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == todo.id).first()
    update_file.title = todo.title
    update_file.details = todo.details
    update_file.tag = str(todo.tag)
    if todo.completed:
        update_file.completed_date = datetime.now().strftime('%Y-%m-%d')
    else:
        update_file.completed_date = None
    update_file.completed = todo.completed
    db.commit()
    db.refresh(update_file)
    db.close()

    return todo


def gen():
    letters = string.ascii_lowercase
    list_tag = ["studies", "personal", "plans", "empty"]
    db = SessionLocal()
    for i in range(20):
        size = random.randint(5, 20)
        title = ''.join(random.choice(letters) for j in range(size))
        details = ''.join(random.choice(letters) for k in range(size))
        tag = list_tag[random.randint(0, 3)]
        # print(title, details, tag)
        # generate_todo(CreateTodo(title=title, details=details, tag=tag))
        td = Todo(title=title, details=details, tag=tag, completed=False)
        db.add(td)
        db.commit()
        db.refresh(td)
    db.close()


def vis():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    db.close()
    data_values = [0, 0, 0, 0]

    data_names = ['Tags.studies', 'Tags.personal', 'Tags.plans', 'Tags.empty']
    for todo in todos:
        data_values[data_names.index(str(todo.tag))] += 1
    dpi = 80
    fig = plt.figure(dpi=dpi, figsize=(512 / dpi, 384 / dpi))
    mpl.rcParams.update({'font.size': 9})

    plt.title('Распределение количества по тэгам (%)')

    xs = range(len(data_names))

    plt.pie(
        data_values, autopct='%.1f', radius=1.1,
        explode=[0.15] + [0 for _ in range(len(data_names) - 1)])
    plt.legend(
        bbox_to_anchor=(-0.16, 0.45, 0.25, 0.25),
        loc='lower left', labels=data_names)
    fig.savefig('./static/img/pie.png')


def export_xlsx():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    db.close()
    workbook = xlsxwriter.Workbook('todo.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.write(0, 0, 'id')
    worksheet.write(0, 1, 'title')
    worksheet.write(0, 2, 'details')
    worksheet.write(0, 3, 'tag')
    worksheet.write(0, 4, 'origin')
    worksheet.write(0, 5, 'completed')
    worksheet.write(0, 6, 'created_date')
    worksheet.write(0, 7, 'completed_date')

    for i, todo in enumerate(todos):
        for j in range(8):
            if j == 0:
                worksheet.write(i + 1, j, todo.id)
            elif j == 1:
                worksheet.write(i + 1, j, todo.title)
            elif j == 2:
                worksheet.write(i + 1, j, todo.details)
            elif j == 3:
                worksheet.write(i + 1, j, str(todo.tag).split(".")[1])
            elif j == 4:
                worksheet.write(i + 1, j, str(todo.origin).split(".")[1])
            elif j == 5:
                worksheet.write(i + 1, j, todo.completed)
            elif j == 6:
                worksheet.write(i + 1, j, todo.created_date)
            elif j == 7:
                worksheet.write(i + 1, j, todo.completed_date)
    workbook.close()

def import_xlsx(filename):
    workbook = openpyxl.load_workbook(filename)

    sheet = workbook.active
    data_dict = {}

    for row in sheet.iter_rows(values_only=True):
        column_name = row[0]
        column_data = row[1:]
        data_dict[column_name] = column_data
    data_dict.pop('id')
    for key in data_dict.keys():
        print(f"data_dict[key][4]: {type(data_dict[key][4])}")
        todo = CreateTodo(title=data_dict[key][0],
                          details=data_dict[key][1],
                          tag=data_dict[key][2],
                          origin=data_dict[key][3],
                          completed=data_dict[key][4],
                          completed_date=data_dict[key][5],
                          created_date=data_dict[key][6])
        create_todo(todo)
    return data_dict

def complete_todo(id: int):
    db = SessionLocal()
    update_file = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == id).first()
    if not update_file.completed:
        update_file.completed_date = datetime.now().strftime('%Y-%m-%d')
    else:
        update_file.completed_date = None
    update_file.completed = not update_file.completed
    db.commit()
    db.refresh(update_file)
    db.close()
